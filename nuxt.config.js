export default {
  components: true,
  head: {
    titleTemplate: ' Mastering Nuxt: %s',
    htmlAttrs: {
      lang: 'en',
    },
    bodyAttrs: {
      class: ['my-style'],
    },
    meta: [
      {
        charset: 'utf-8',
      },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // Preload fonts
      // {
      //   rel: 'preload',
      //   as: 'font',
      //   crossorigin: 'crossorigin',
      //   type: 'font/woff2',
      //   href: '/fonts/some-font.woff2',
      // },
    ],
  },

  css: ['~/assets/scss/app.scss'],
};
